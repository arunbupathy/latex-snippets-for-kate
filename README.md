# Latex code snippets for Kate editor

Some latex code snippets that I personally use with KDE's text editor Kate.

To use these latex snippets in Kate, copy the XML file to `~/.local/share/ktexteditor_snippets/data/`
